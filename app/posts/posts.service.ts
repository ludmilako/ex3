import { Injectable } from '@angular/core';

@Injectable()
export class PostsService {

    posts = [
    {author :'John Kenady',title:'Politics',content:'contact with me john@gmail.com'},
    {author :'Jack Wolfson',title:'Travel',content:'contact with me jack@gmail.com'},
    {author:'Alice DJ',title:'Music',content:'contact with me alice@gmail.com'},
    {author :'Sheldon Cooper',title:'Science',content:'contact with me sheldon@gmail.com'}
  ]

   getPosts(){
		return this.posts;
	}

  constructor() { }

}
